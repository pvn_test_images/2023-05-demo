# 2023-05-demo

Simple demo for Anchore Enterprise, including both Jenkins and GitLab workflow examples.
Partial list of conditions that can be tested with this image:

* xmrig cryptominer installed at /xmrig/xmrig
* simulated AWS access key in /aws_access
* simulated ssh private key in /ssh_key
* selection of commonly-blocked packages installed (sudo, curl, etc)
* /log4j-core-2.14.1.jar (CVE-2021-44228, et al)
* added anchorectl to demonstrate automatic go module detection (new in syft 0.42.0)
* wide variety of ruby, node, python, java installed with different licenses
